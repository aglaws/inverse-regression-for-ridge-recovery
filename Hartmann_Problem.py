import numpy as np
import active_subspaces
import matplotlib.pyplot as plt

# Set number of inputs (m) and samples (N)
m, N = 5, 10**6

# Generate inputs and compute B_ind
x = np.random.normal(size=(N, m))

mu_mean = np.array([-2.25, 1, 0.3, 0.3, -0.75])
sigma = np.array([0.15, 0.25, 0.25, 0.25, 0.25])
x_Hart = np.exp(x*sigma + mu_mean)

mu = x_Hart[:,0].reshape((N, 1))
rho = x_Hart[:,1].reshape((N, 1))
dpdx = x_Hart[:,2].reshape((N, 1))
eta = x_Hart[:,3].reshape((N, 1))
B0 = x_Hart[:,4].reshape((N, 1))

Ha = B0/np.sqrt(eta*mu)
B_ind = dpdx*(B0 - 2*np.sqrt(eta*mu)*np.tanh(Ha/2))/(2*B0**2)

# Set SDR Subspace
for SDR_sub in ['SIR', 'SAVE']:

    # Estimate true central ridge subspace using either SIR or SAVE
    sub = active_subspaces.subspaces.Subspaces()
    sub.compute(X=x, f=B_ind, sstype=SDR_sub, nboot=100)
    
    y = np.dot(x, sub.eigenvecs)

    # Plotting information
    fontsize_label = 24
    fontsize_ticks = 18
    N_plot = 500
    file_type = 'png'
    file_type2 = 'eps'

    # Plot eigenvalues of estimated SIR/SAVE matrix with bootstrap bounds
    plt.figure(figsize=(7,7))
    plt.semilogy(range(1 ,m+1), sub.eigenvals, 'ko-', markersize=12, linewidth=2)
    plt.fill_between(range(1, m+1), np.minimum(sub.e_br[:, 0].reshape((m, 1)), sub.eigenvals).reshape((m,)), 
        np.maximum(sub.e_br[:, 1].reshape((m, 1)), sub.eigenvals).reshape((m,)),
        facecolor='0.7', interpolate=True)
    plt.xlabel('Index', fontsize=fontsize_label)
    plt.ylabel('Eigenvalues', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks(range(1, m+1))
    plt.tight_layout()
    figname = 'figs/hartmann_Bind_' + SDR_sub + '_evals.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
    
    # Plot SIR/SAVE subspace error with bootstrap bounds
    plt.figure(figsize=(7,7))
    plt.semilogy(range(1, m), sub.sub_br[:, 1], 'ko-', markersize=12)
    plt.fill_between(range(1, m), sub.sub_br[:, 0], sub.sub_br[:, 2], facecolor='0.7', interpolate=True)
    plt.xlabel('Subspace dimension', fontsize=fontsize_label)
    plt.ylabel('Subspace distance', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks(range(1, m))
    plt.axis([1, m-1, 0.1*np.amin(sub.sub_br[:,0]), 1])
    plt.tight_layout()
    figname = 'figs/hartmann_Bind_' + SDR_sub + '_suberr.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
    
    # Plot 1d sufficient summary plot
    plt.figure(figsize=(7,7))
    plt.plot(y[:N_plot, 0], B_ind[:N_plot], 'bo', markersize=12)
    plt.xlabel(r'$\hat{\mathbf{w}}_1^T \mathbf{x}$', fontsize=fontsize_label)
    plt.ylabel(r'$\mathrm{B}_{\mathrm{ind}}$', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.tight_layout()
    figname = 'figs/hartmann_Bind_' + SDR_sub + '_1d_ssp.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
    
    # Plot 2d sufficient summary plot
    plt.figure(figsize=(7,7))
    plt.scatter(y[:N_plot, 0], y[:N_plot, 1], c=B_ind[:N_plot], s=150.0, vmin=np.min(B_ind[:N_plot]), vmax=np.max(B_ind[:N_plot]))
    plt.xlabel(r'$\hat{\mathbf{w}}_1^T \mathbf{x}$', fontsize=fontsize_label)
    plt.ylabel(r'$\hat{\mathbf{w}}_2^T \mathbf{x}$', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    ymin = 1.1*np.amin([np.amin(y[:N_plot, 0]), np.amin(y[:N_plot, 1])])
    ymax = 1.1*np.amax([np.amax(y[:N_plot, 0]), np.amax(y[:N_plot, 1])])
    plt.axis([ymin, ymax, ymin, ymax])
    plt.axes().set_aspect('equal')
    cbar = plt.colorbar(shrink=0.675)
    for t in cbar.ax.get_yticklabels():
        t.set_fontsize(fontsize_ticks)
    cbar.set_label(r'$\mathrm{B}_{\mathrm{ind}}$', size=fontsize_label)
    plt.tight_layout()
    figname = 'figs/hartmann_Bind_' + SDR_sub + '_2d_ssp.eps'
    plt.savefig(figname, dpi=300)
    plt.close()