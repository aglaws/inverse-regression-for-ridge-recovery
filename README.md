Scripts for generating figures for the paper 

'Inverse regression for ridge recovery'
A. Glaws, P. G. Constantine, and R. D. Cook
https://arxiv.org/abs/1702.02227

These scripts require the dev branch of the Python Active-subspaces Utility Library available here:
https://github.com/paulcon/active_subspaces/tree/dev