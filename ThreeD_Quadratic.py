import numpy as np
import active_subspaces
import random
import matplotlib.pyplot as plt

# Set number of inputs (m) and samples (N)
m, N = 10, 10**7

# Generate B (mx2) and b (mx1) for f(x) = X^T B B^T X + b^T X
np.random.seed(42)
B = np.dot(2*np.random.uniform(size=(m, 2)) - 1, np.diag([1, 0.5]))
b = 2*np.random.uniform(size=(m, 1)) - 1
BBT = np.dot(B, B.T)

# Set SDR Subspace
for SDR_sub in ['SIR', 'SAVE']:
    
    # Set number of iterations (n_iters) and subsampling sizes (N_sub)
    n_iters = 10
    N_sub = [5**2, 5**3, 5**4, 5**5, 5**6, 5**7, 5**8, 5**9]
    eig_errors = np.zeros((len(N_sub), n_iters))
    sub_errors = np.zeros((len(N_sub), n_iters))
    for j in range(n_iters):
        
        # Compute f(x)
        np.random.seed(4242)
        x = np.random.normal(size=(N, m))
        f = np.zeros((N, 1))
        for i in range(N):
            x_i = x[i, :].reshape(m, 1)
            f[i] = np.dot(np.dot(x_i.T, BBT), x_i) + np.dot(b.T, x_i)
    
        # Estimate true central ridge subspace using either SIR or SAVE
        sub_true = active_subspaces.subspaces.Subspaces()
        sub_true.compute(X=x, f=f, sstype=SDR_sub)
        
        # Subsample N data points to test convergence
        for i in range(len(N_sub)):
            ind = random.sample(range(N), N_sub[i])
        
            # Estimate central ridge subspace using either SIR or SAVE
            sub = active_subspaces.subspaces.Subspaces()
            sub.compute(X=x[ind, :], f=f[ind, :], sstype=SDR_sub)
            
            # Compute max squared eigenvalue error normalized by largest eigenvalue
            eig_errors[i, j] = (np.max(np.abs(sub.eigenvals - sub_true.eigenvals))/sub_true.eigenvals[0])**2 
            
            # Compute subspace error for n = 3
            sub_errors[i, j] = np.linalg.norm(np.dot(sub_true.eigenvecs[:, :3].T, sub.eigenvecs[:, 3:]), ord=2)
    
    # Compute asymptotic convergence rate for max squared eigenvalue errors
    A = np.vstack([np.log(N_sub), np.ones(len(N_sub))]).T
    y = np.log(np.mean(eig_errors, axis=1).reshape(len(N_sub), 1))
    eig_conv_rate, junk = np.linalg.lstsq(A, y)[0]
    print SDR_sub + ' eigenvalue convergence rate: ' + str(eig_conv_rate)
    
    # Plotting information
    fontsize_label = 24
    fontsize_ticks = 18
    
    # Plot eigenvalues of estimated SIR/SAVE matrix
    plt.figure(figsize=(7,7))
    plt.semilogy(range(1 ,m+1), sub.eigenvals, 'ko-', markersize=12, linewidth=2)
    plt.xlabel('Index', fontsize=fontsize_label)
    plt.ylabel('Eigenvalues', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks(range(1, m+1))
    plt.tight_layout()
    figname = 'figs/quadratic2_' + SDR_sub + '_evals.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
            
    # Plot max squared eigenvalue errors
    plt.figure(figsize=(7,7))
    for j in range(n_iters):
        plt.loglog(N_sub, eig_errors[:, j], 'ko-', markersize=12, linewidth=2)
    plt.xlabel(r'$N$', fontsize=fontsize_label)
    plt.ylabel('Max Squared Eigenvalue Error', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks([10**1, 10**2, 10**3, 10**4, 10**5, 10**6, 10**7])
    plt.tight_layout()
    figname = 'figs/quadratic2_' + SDR_sub + '_eval_errs.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
    
    # Plot subspace errors for n = 3
    plt.figure(figsize=(7,7))
    for j in range(n_iters):
        plt.loglog(N_sub, sub_errors[:, j], 'ko-', markersize=12, linewidth=2)
    plt.xlabel(r'$N$', fontsize=fontsize_label)
    plt.ylabel('Subspace Error', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks([10**1, 10**2, 10**3, 10**4, 10**5, 10**6, 10**7])
    plt.yticks([10**(-3), 10**(-2), 10**(-1), 10**0])
    plt.tight_layout()
    figname = 'figs/quadratic2_' + SDR_sub + '_sub_errs.eps'
    plt.savefig(figname, dpi=300)
    plt.close()