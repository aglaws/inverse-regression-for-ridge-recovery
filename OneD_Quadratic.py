import numpy as np
import active_subspaces
import matplotlib.pyplot as plt

# Set number of inputs (m) and samples (N)
m, N = 10, 10**6

# Generate b (mx1) for f(x) = b^T x
np.random.seed(42)
b = np.random.normal(size=(m, 1))
x = np.random.normal(size=(N, m))

f = np.dot(x, b)**2

# Set SDR Subspace
for SDR_sub in ['SIR', 'SAVE']:
    
    # Estimate central ridge subspace using either SIR or SAVE
    sub = active_subspaces.subspaces.Subspaces()
    sub.compute(X=x, f=f, sstype=SDR_sub)
    
    y = np.dot(x, sub.eigenvecs)
    
    # Plotting information
    fontsize_label = 24
    fontsize_ticks = 18
    N_plot = 300

    # Plot eigenvalues of estimated SIR/SAVE matrix
    plt.figure(figsize=(7,7))
    plt.semilogy(range(1 ,m+1), sub.eigenvals, 'ko-', markersize=12, linewidth=2)
    plt.xlabel('Index', fontsize=fontsize_label)
    plt.ylabel('Eigenvalues', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.xticks(range(1, m+1))
    plt.tight_layout()
    figname = 'figs/quadratic1_' + SDR_sub + '_evals.eps'
    plt.savefig(figname, dpi=300)
    plt.close()
    
    # Plot 1d sufficient summary plot
    plt.figure(figsize=(7,7))
    plt.plot(y[:N_plot, 0], f[:N_plot], 'bo', markersize=12)
    plt.xlabel(r'$\hat{\mathbf{w}}_1^T \mathbf{x}$', fontsize=fontsize_label)
    plt.ylabel(r'$y$', fontsize=fontsize_label)
    plt.grid(True)
    plt.tick_params(labelsize=fontsize_ticks)
    plt.tight_layout()
    figname = 'figs/quadratic1_' + SDR_sub + '_1d_ssp.eps'
    plt.savefig(figname, dpi=300)
    plt.close()